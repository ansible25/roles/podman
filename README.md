Podman
=========

Role that installs Podman

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Podman on targeted machine:

    - hosts: servers
      roles:
         - podman

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
